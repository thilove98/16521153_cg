#include "Ellipse.h"

void Draw4Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	SDL_RenderDrawPoint(ren, xc+x, yc+y);
	SDL_RenderDrawPoint(ren, xc + x, yc - y);
	SDL_RenderDrawPoint(ren, xc - x, yc + y);
	SDL_RenderDrawPoint(ren, xc - x, yc - y);
}

void BresenhamDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
	// Area 1
	Draw4Points(xc, yc, 0, b, ren);
	int x = 0, y = b, p = -2 * a*a*b + a * a + 2 * b*b, t = 4 * a*a + 6 * b*b;
	for (; (a*a)*(a*a) >= x * x*(a*a + b * b); x++)
	{
		if (p <= 0)p += 4 * b*b*x + 6 * b*b; else
		{
			p += 4 * b*b*x - 4 * a*a*y + t;
			y--;
		}
		Draw4Points(xc, yc, x, y, ren);
	}
	// Area 2
	Draw4Points(xc, yc, a, 0, ren);
	x = a, y = 0, p = -2 * b*b*a + b * b + 2 * a*a, t = 4 * b*b + 6 * a*a;
	for (; (b*b)*(b*b) >= y * y*(b*b + a * a); y++)
	{
		if (p <= 0)p += 4 * a*a*y + 6 * a*a; else
		{
			p += 4 * a*a*y - 4 * b*b*x + t;
			x--;
		}
		Draw4Points(xc, yc, x, y, ren);
	}
}

void MidPointDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
	// Area 1
	Draw4Points(xc, yc, 0, b, ren);
	int x = 0, y = b, p = a * a + 4 * b*b - 4*a * a*b, i = 12 * b*b, j = 8 * a*a - 8 * a*a*b + 12 * b*b;
	for (; (a*a)*(a*a) >= x * x*(a*a + b * b); x++) {
		if (p < 0)
		{
			p += i;
			i += 8 * b*b;
			j += 8 * b*b;
		}
		else
		{
			y--;
			p += j;
			i += 8 * b*b;
			j += 8 * a*a + 8 * b*b;
		}
		Draw4Points(xc, yc, x, y, ren);
	}
	// Area 2
	Draw4Points(xc, yc, a, 0, ren);
	x = a, y = 0, p = b * b + 4 * a*a - 4 * b * b*a, i = 12 * a*a, j = 8 * b*b - 8 * b*b*a + 12 * a*a;
	for (; (b*b)*(b*b) >= y * y*(b*b + a * a); y++) {
		if (p < 0)
		{
			p += i;
			i += 8 * a*a;
			j += 8 * a*a;
		}
		else
		{
			x--;
			p += j;
			i += 8 * a*a;
			j += 8 * b*b + 8 * a*a;
		}
		Draw4Points(xc, yc, x, y, ren);
	}
}