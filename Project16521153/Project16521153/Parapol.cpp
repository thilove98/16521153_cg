#include "Parapol.h"
void Draw2Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
    //draw 2 points
	SDL_RenderDrawPoint(ren, x + xc, y + yc);
	SDL_RenderDrawPoint(ren, -x + xc, y + yc);
}
void BresenhamDrawParapolPositive(int xc, int yc, int A, SDL_Renderer *ren)
{
	if (A > 0)A = -A;
	int w, h;
	SDL_GetRendererOutputSize(ren, &w, &h);
	int x = 0, y = 0, p = A;
	Draw2Points(xc, yc, x, y, ren);
	for (; x <= -A; x++)
	{
		if (p > 0)p -= 2 * x + 1; else
		{
			p -= 2 * x + 1 - 2 * (-A);
			y--;
		}
		Draw2Points(xc, yc, x, y, ren);
	}
	p = 4 * A*y - 2 * x*x - 2 * x - 1;
	Draw2Points(xc, yc, x, y, ren);
	for (; y+yc>0; y--)
	{
		if (p < 0)p += -4 * A; else 
		{
			p += -4 * A - 4 * x - 4;
			x++;
		}
		Draw2Points(xc, yc, x, y, ren);
	}
}

void BresenhamDrawParapolNegative(int xc, int yc, int A, SDL_Renderer *ren)
{
	if (A < 0)A = -A;
	int w, h;
	SDL_GetRendererOutputSize(ren, &w, &h);
	int x = 0, y = 0, p = -A;
	Draw2Points(xc, yc, x, y, ren);
	for (; x <= A; x++)
	{
		if (p < 0)p += 2 * x + 1; else
		{
			p += 2 * x + 1 - 2 * A;
			y++;
		}
		Draw2Points(xc, yc, x, y, ren);
	}
	p = 4 * A*y - 2 * x*x - 2 * x - 1;
	Draw2Points(xc, yc, x, y, ren);
	for (; y + yc <= h; y++)
	{
		if (p < 0)p += 4 * A; else 
		{
			p += 4 * A - 4 * x - 4;
			x++;
		}
		Draw2Points(xc, yc, x, y, ren);
	}
}