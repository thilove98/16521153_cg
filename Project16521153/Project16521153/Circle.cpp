#include "Circle.h"

void Draw8Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	int a[8] = { x,-x,x,-x,y,-y,y,-y };
	int b[8] = { y,-y,-y,y,x,-x,-x,x };
	for (int i = 0; i < 8;i++)SDL_RenderDrawPoint(ren, a[i] + xc, b[i] + yc);
}

void BresenhamDrawCircle(int xc, int yc, int R, SDL_Renderer *ren)
{
	int c, x = 0, y = R, p = 3 - 2 *R;
	for (;x<=y;x++) {
		if (p<0) p=p+4*x+6;
		else {
			p=p+(x - y)*4+10;
			y--;
		}
		Draw8Points(xc, yc,x,y,ren);
	}
}

void MidpointDrawCircle(int xc, int yc, int R, SDL_Renderer *ren)
{
	int y = R,x = 0,p = 1 - R;
	Draw8Points(xc, yc, x, y, ren);
	for(;x<y;)
	{
		if (p < 0) p += (x<<1) + 3;else
		{
			y--;
			p += ((x - y) <<1) + 5;
		}
		Draw8Points(xc, yc, x, y, ren);
		x++;
	}
}
