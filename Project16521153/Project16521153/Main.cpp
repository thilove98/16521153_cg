#include <iostream>
#include <SDL.h>
#include "Bezier.h"
#include "FillColor.h"

using namespace std;

const int WIDTH = 800;
const int HEIGHT = 1000;

SDL_Event event;


int main(int, char**) {
	//First we need to start up SDL, and make sure it went ok
	if (SDL_Init(SDL_INIT_VIDEO) != 0) {
		std::cout << "SDL_Init Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	SDL_Window *win = SDL_CreateWindow("Hello World!", 0, 0, WIDTH, HEIGHT, SDL_WINDOW_SHOWN);
	//Make sure creating our window went ok
	if (win == NULL) {
		std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	//Create a renderer that will draw to the window, -1 specifies that we want to load whichever
	//DON'T FORGET CHANGE THIS LINE IN YOUR SOURCE ----->>>>> SDL_RENDERER_SOFTWARE

	SDL_Renderer *ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_SOFTWARE);
	if (ren == NULL) {
		SDL_DestroyWindow(win);
		std::cout << "SDL_CreateRenderer Error: " << SDL_GetError() << std::endl;
		SDL_Quit();
		return 1;
	}

	SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
	SDL_RenderClear(ren);

	//YOU CAN INSERT CODE FOR TESTING HERE

	/*Vector2D v1(100, 200);
	Vector2D v3(200, 00);
	Vector2D v2(300, 300);*/

	Vector2D v1(300, 300);
	Vector2D v3(400, 100);
	Vector2D v2(100, 400);

	SDL_SetRenderDrawColor(ren, 255, 255, 255, 255);

	SDL_Color color;
	color.r = 128;
	color.g = 0;
	color.b = 128;
	color.a = 255;

	SDL_SetRenderDrawColor(ren, 255, 0, 0, 0);
	TriangleFill(v1, v2, v3, ren, color);
	//RectangleFill({ 100,400 }, { 300,500 }, ren, color);
	//CircleFill(100, 100, 100, ren, color);
	//CircleFill(100, 200, 100, ren, color);
	FillIntersectionEllipseCircle(200, 100, 100, 200, 200, 200, 100, ren, color);
	FillIntersectionTwoCircles(100, 100, 100, 200, 100, 50, ren, color);
	FillIntersectionRectangleCircle({ 100,400 }, { 300,500 }, 300, 350, 100, ren, color);

	SDL_SetRenderDrawColor(ren, 255, 255, 255, 255);
	DrawCurve2(ren, { 0,500 }, { 300,00 }, { 800,500 });
	DrawCurve3(ren, { 200,600 }, { 100,200 }, { 500,200 }, { 700,600 });
	SDL_RenderPresent(ren);
	//Take a quick break after all that hard work
	//Quit if happen QUIT event
	SDL_Delay(2000);
	bool running = true;

	DrawCurve3(ren, { 200,600 }, { 100,200 }, { 500,200 }, { 700,600 });
	SDL_Rect r[4];
	r[0] = { 200,600, 15,15 };
	r[1] = { 100,200, 15,15 };
	r[2] = { 500,200, 15,15 };
	r[3] = { 700,600, 15,15 };
	int n = 0, ismove = 0;
	while (running)
	{

		//If there's events to handle
		SDL_Event event;
		if (SDL_PollEvent(&event))
		{
			int x = 0, y = 0;
			SDL_GetMouseState(&x, &y);
			//If the user has Xed out the window
			if (event.type == SDL_QUIT)
			{
				//Quit the program
				running = false;
			}
			if (event.type == SDL_MOUSEBUTTONDOWN)if (event.button.button == SDL_BUTTON_LEFT)
				for (int i = 0; i <= 3; i++)if (x >= r[i].x&&y >= r[i].y&&x <= r[i].x + r[i].w&&y < r[i].y + r[i].h)
				{
					ismove = 1;
					n = i;
				}

			if (ismove)
			{
				r[n].x = x;
				r[n].y = y;
			}
			if (event.type == SDL_MOUSEBUTTONUP)if (event.button.button == SDL_BUTTON_LEFT)ismove = 0;
		}

		SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
		SDL_RenderClear(ren);

		SDL_SetRenderDrawColor(ren, 0, 138, 0, 255);
		for (int i = 0; i < 3; i++)DDA_Line(r[i].x, r[i].y, r[i + 1].x, r[i + 1].y, ren);

		SDL_SetRenderDrawColor(ren, 255, 255, 255, 255);
		Vector2D v(r[0].x, r[0].y);
		DrawCurve3(ren, Vector2D(r[0].x, r[0].y), Vector2D(r[1].x, r[1].y), Vector2D(r[2].x, r[2].y), Vector2D(r[3].x, r[3].y));

		SDL_SetRenderDrawColor(ren, 255, 0, 0, 255);
		for (int i = 0; i < 4; i++)
			SDL_RenderFillRect(ren, &r[i]);

		SDL_RenderPresent(ren);
	}

	SDL_DestroyRenderer(ren);
	SDL_DestroyWindow(win);
	SDL_Quit();

	return 0;
}
