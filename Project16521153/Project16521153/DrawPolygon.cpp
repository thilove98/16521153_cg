#include "DrawPolygon.h"
#include <iostream>
using namespace std;

void DrawEquilateralTriangle(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[3], y[3];
	float phi = 2 * M_PI / 3,delta=phi;
	for (int i = 0; i <3; i++)
	{
		x[i] = round(- R * sin(phi));
		y[i] = round(R * cos(phi));
		phi += delta;
	}
	for(int i=0;i<3;i++)
	Midpoint_Line(x[i]+xc, -y[i]+yc, x[(i+1)%3]+xc, -y[(i+1)%3]+yc, ren);
}

void DrawSquare(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[4], y[4];
	float phi = M_PI/2,delta=phi;
	for (int i = 0; i <4; i++)
	{
		x[i] = round(- R * sin(phi));
		y[i] = round( R * cos(phi));
		phi += delta;
	}
	for (int i = 0; i<4; i++)
		Midpoint_Line(x[i] + xc, y[i] + yc, x[(i + 1)%4] + xc, y[(i + 1)%4] + yc, ren);
}
void DrawPentagon(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[5], y[5];
	float phi = 2*M_PI / 5,delta=phi;

	for (int i = 0; i <5; i++)
	{
		x[i] = round(-R * sin(phi));
		y[i] = round(R * cos(phi));
		phi += delta;
	}
	for (int i = 0; i < 5; i++)y[i] = -y[i];
	for (int i = 0; i<5 ; i++)
		Midpoint_Line(x[i] + xc, y[i] + yc, x[(i + 1)%5] + xc, y[(i + 1)%5] + yc, ren);
}
void DrawHexagon(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[6], y[6];
	float phi = M_PI / 3,delta=phi;

	for (int i = 0; i <6; i++)
	{
		x[i] = round(- R * sin(phi));
		y[i] = round(R * cos(phi));
		phi += delta;
	}
	for (int i = 0; i<6 ; i++)Midpoint_Line(x[i] + xc, y[i] + yc, x[(i + 1)%6] + xc, y[(i + 1)%6] + yc, ren);
}

void DrawStar(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[5], y[5];
	float phi = 2 * M_PI / 5,delta=phi;
	for (int i = 0; i <5; i++)
	{
		x[i] = round( R* sin(phi));
		y[i] = round( -R* cos(phi));
		phi += delta;
	}
	for (int i = 0; i < 5; i++)Midpoint_Line(x[i]+xc, y[i]+yc, x[(i + 2)%5]+xc, y[(i + 2)%5]+yc, ren);
}

void DrawEmptyStar(int xc, int yc, int R, SDL_Renderer *ren)
{
	double  delta = 2 * M_PI / 5, phi = M_PI/2, r = sin(M_PI / 10)*R / sin(7 * M_PI / 10);
	double phi2 = phi + M_PI / 5;

	int x[10], y[10];
	for (int i = 0; i < 5; i++)
	{
		x[i] = xc + round(R*cos(phi));
		y[i] = yc - round(R*sin(phi));

		x[i + 5] = xc + round(r*cos(phi2));
		y[i + 5] = yc - round(r*sin(phi2));
		phi += delta;
		phi2 += delta;
	}
	for (int i = 0; i < 5; i++)
	{
		Midpoint_Line(x[i], y[i], x[i + 5], y[i + 5], ren);
		Midpoint_Line(x[i + 5], y[i + 5], x[(i + 1) % 5], y[(i + 1) % 5], ren);
	}
}

//Star with eight wings
void DrawStarEight(int xc, int yc, int R, SDL_Renderer *ren)
{
	double const delta = 2 * M_PI / 8;
	double phi = M_PI / 2;
	double r = sin(M_PI / 8)*R / sin(6 * M_PI / 8);
	double phi2 = phi + M_PI / 8;

	int x[16],y[16];
	for (int i = 0; i < 8; i++)
	{
		x[i] = xc + round(R*cos(phi));
		y[i] = yc - round(R*sin(phi));

		x[i+8] = xc + round(r*cos(phi2));
		y[i+8] = yc - round(r*sin(phi2));
		phi += delta;
		phi2 += delta;
	}
	for (int i = 0; i < 8; i++)
	{
		Midpoint_Line(x[i], y[i], x[i+8], y[i+8], ren);
		Midpoint_Line(x[i+8], y[i+8], x[(i + 1) % 8], y[(i + 1) % 8], ren);
	}
	
}

//For drawing one star of convergent star
void DrawStarAngle(int xc, int yc, int R, float startAngle, SDL_Renderer *ren)
{
	double  delta = 2 * M_PI / 5, phi = startAngle, r = sin(M_PI / 10)*R / sin(7 * M_PI / 10);
	double phi2 = phi + M_PI / 5;

	int x[10], y[10];
	for (int i = 0; i < 5; i++)
	{
		x[i] = xc + round(R*cos(phi));
		y[i] = yc - round(R*sin(phi));

		x[i+5] = xc + round(r*cos(phi2));
		y[i+5] = yc - round(r*sin(phi2));
		phi += delta;
		phi2 += delta;
	}
	for (int i = 0; i < 5; i++)
	{
		Midpoint_Line(x[i], y[i], x[i+5], y[i+5], ren);
		Midpoint_Line(x[i+5], y[i+5], x[(i + 1) % 5], y[(i + 1) % 5], ren);
	}

}

void DrawConvergentStar(int xc, int yc, int r, SDL_Renderer *ren)
{
	double phi = M_PI / 2;
	while (r > 1)
	{
		DrawStarAngle(xc, yc, r, phi, ren);
		r = sin(M_PI / 10)*r / sin(7 * M_PI / 10);
		phi = phi + M_PI;
	}
}
